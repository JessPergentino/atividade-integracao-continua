package fatorial;

public class CalculoFatorial {

	public int calcularFatorial(int num){
		if (num <=1 )  return 1;
		else  return num * calcularFatorial(num - 1);
	}
}
